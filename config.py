from pydantic import BaseSettings
import os


class Settings(BaseSettings):
    APP_NAME: str = 'heart'
    URL_PREFIX_V1: str = '/api/v1'
    PORT: int = 50005
    ENV: str = 'prod'
    DB_PORT: int = 5432
    DB_HOST: str = '188.43.234.137'
    DB_USER: str = 'true24'
    DB_PASS: str = 'Serial111'
    DB_DATABASE: str = 'heart'
    CLOUD_PATH: str =  os.path.join(os.getcwd(),'models')

    class Config:
        def __init__(self):
            pass

        env_prefix = 'gaz_'


settings = Settings()
from typing import Optional
from pydantic import BaseModel
from datetime import date

class User(BaseModel):
    username: str
    password: str

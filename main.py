from fastapi import FastAPI, Depends, Header, HTTPException, status, Response, Request
from typing import Optional
import uvicorn
from starlette.middleware.cors import CORSMiddleware
import psycopg2
from psycopg2.extras import DictCursor, RealDictCursor
import json, uuid, os
from datetime import datetime, timedelta, date

from config import settings
from classes.user import User
# from app.api.api_v1.urls import api_v1_router
# from app.scikit_model.scoring_ml_module import ScoreModel


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=["*"],
    allow_credentials=True,
    allow_headers=["*"]
)


@app.on_event("startup")
async def startup_event():
    """ Loading ML model """
    # Connect to postgres

    global model
    print('Start loading ML model')
#    ScoreModel()
    print('Done')



############################ Поинты авторизации ################################
@app.post(settings.URL_PREFIX_V1 + "/login")
async def login(user: User):
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                SELECT id, first_name, second_name, role
                FROM users
                WHERE username='{user.username}' and password='{user.password}'
            ''')
            user_profile = cursor.fetchone()
            if not user_profile:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Invalid authentication credentials",
                )

    return user_profile
########################## end Поинты авторизации ##############################


########################## Поинты работы с графиками ###########################
@app.get(settings.URL_PREFIX_V1 + "/patients")
async def patients():
    """ Получение списка пациентов
    """
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                SELECT *
                FROM users
                WHERE role='Пациент'
            ''')
            metrics = cursor.fetchall()
    return metrics


@app.get(settings.URL_PREFIX_V1 + "/factor_risk")
async def factor_risk():
    """ Получение риска заболевания СС в зависимости от фактора
    """
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                SELECT factor,
                       arterial_hypertension_percent,
                       onmk_percent,
                       ibs_percent,
                       heart_failure_percent,
                       other_heart_diseases_percent
                FROM factors_risks
            ''')
            metrics = cursor.fetchall()
    return metrics

@app.get(settings.URL_PREFIX_V1 + "/patients_groups")
async def patients_groups():
    """ Получение количества пациентов с определенным заболеванием
    """
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                SELECT count(arterial_hypertension_duration) as "Артериальная гипертензия",
                	   count(onmk_duration) as "ОНМК",
                	   count(angina_pectoris_duration) as "Стенокардия, ИБС, инфаркт миокарда",
                	   count(heart_failure_duration) as "Сердечная недостаточность",
                	   count(other_heart_diseases_duration) as "Прочие заболевания сердца",
                	   count(diabetes_duration) as "Сахарный диабет",
                	   count(hepatitis_duration) as "Гепатит",
                	   count(oncology_duration) as "Онкология"
                FROM report_1 r2
            ''')
            metrics = cursor.fetchone()
    return metrics


def get_patient_profile(patient_id='1'):
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                select upper_pressure||'/'||under_pressure as bloodPresure,
                	   glucose_level as glucoseLevel,
                	   cholesterol as cholesterol,
                	   risk
                from patient_profile pp, users u2
                where pp.id=u2.id
            ''')
            metrics = cursor.fetchone()

        return metrics


@app.get(settings.URL_PREFIX_V1 + "/patient_dashboard")
async def patient_dashboard():
    """ Получение количества пациентов с определенным заболеванием
    """
    with open('patient_dash.json','r', encoding='utf-8') as file:
        data = json.load(file, encoding='utf-8')
        data['profile'] = get_patient_profile()
        return data

def get_sex_stats():
    """ Получение данных о количестве поциентов с ССЗ в зависимости от пола
    """
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                select array_agg(arterial) as "Артериальная гипертензия",
                	   array_agg(onmk) as "ОНМК",
                	   array_agg(angina) as "Стенокардия, ИБС, инфаркт миокарда",
                	   array_agg(failure) as "Сердечная недостаточность",
                	   array_agg(other) as "Прочие заболевания сердца",
                	   array_agg(str_sex) as "Пол"
                from (
                	select count(arterial_hypertension_duration) as arterial,
                		   count(onmk_duration) as onmk,
                		   count(angina_pectoris_duration) as angina,
                		   count(heart_failure_duration) as failure,
                		   count(other_heart_diseases_duration) as other,
                		   case when sex='М' then 'Мужчины'
                		   		when sex='Ж' then 'Женщины'
                		   end as str_sex
                	from report_1 r2
                	where sex notnull
                	group by sex
                ) as counts
            ''')
            results = cursor.fetchone()
            data = []
            if results:
                categories = results['Пол']
                del results['Пол']

                for key, value in results.items():
                    data.append({
                        'name': key,
                        'data':value
                    })
                return {'data':data,'categories':categories}

def get_factors_stats():
    """ Получение статистики для ССЗ по комплексам факторов
    """
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                select complex,
                	   arterial_hypertension_percent,
                	   onmk_percent,
                	   heart_failure_percent,
                	   ibs_percent,
                	   other_heart_diseases_percent
                from factors_statistic
            ''')
            results = cursor.fetchall()
            categories, arterial, onmk, failure, ibs, other = [], [], [], [], [], []
            for item in results:
                categories.append(item['complex'])
                arterial.append(item['arterial_hypertension_percent'])
                onmk.append(item['onmk_percent'])
                failure.append(item['heart_failure_percent'])
                ibs.append(item['ibs_percent'])
                other.append(item['other_heart_diseases_percent'])

            stats =  {
                "data":[
                    {
                        "name":"Артериальная гипертензия",
                        "data":arterial
                    },
                    {
                        "name":"ОНМК",
                        "data":onmk
                    },
                    {
                        "name":"Стенокардия, ИБС, инфаркт миокарда",
                        "data":ibs
                    },
                    {
                        "name":"Сердечная недостаточность",
                        "data":failure
                    },
                    {
                        "name":"Прочие заболевания сердца",
                        "data":other
                    }
                ],
                "categories":categories
            }
            return stats


@app.get(settings.URL_PREFIX_V1 + "/doctor_dashboard")
async def doctor_dashboard():
    """ Получение данных для дашборда врача
    """
    with open('doctor_dash.json','r', encoding='utf-8') as file:
        data = json.load(file, encoding='utf-8')

        data['factorsStatistic'] = get_factors_stats()
        data['sexStatistic'] = get_sex_stats()
        return data

def get_patients_survay(date=datetime.now()):
    """ Получение статистики по количеству пациентов с ССЗ за
    текущий месяц
    """
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                select array_agg("first") as "Артериальная гипертензия",
                	   array_agg("second") as "ОНМК",
                	   array_agg(third) as "Стенокардия, ИБС, инфаркт миокарда",
                	   array_agg(fourth) as "Сердечная недостаточность",
                	   array_agg(other) as "Прочие заболевания сердца",
                	   array_agg(category) as periods
                from patients_survay ps
            ''')
            results = cursor.fetchone()

            data = []
            if results:
                categories = results['periods']
                del results['periods']

                for key, value in results.items():
                    data.append({
                        'name': key,
                        'data':value
                    })
                return {'data':data,'categories':categories}

def get_hospital_preventions(date=datetime.now()):
    """ Получение списка профилактических мер за текущий месяц
    """
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                select name,
                       status,
                       factors,
                       result_status
                from preventions
            ''')
            metrics = cursor.fetchall()
        return metrics

def get_receipt_doctor_time(date=datetime.now()):
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                select u2.first_name ||' '|| u2.second_name as "Врач",
                	   array_agg("time") as "Время в мин.",
                	   array_agg("date") as categories
                from receipt_time rt
                left join users u2 on rt.doctor_id=u2.id
                group by doctor_id,first_name,second_name
            ''')
            metrics = cursor.fetchall()
            if metrics:
                categories = metrics[0]['categories']
                data = []
                for item in metrics:
                    data.append({
                        "name":item['Врач'] ,
                        "data":item['Время в мин.']
                    })

            return {'data':data,'categories':categories}

def get_death_percent():
    """ Смертность от ССЗ
    """
    return {'data':[105,98,107,100,85],
            'categories':["Июль 2020",
                        "Август 2020",
                        "Сентябрь 2020",
                        "Октябрь 2020",
                        "Ноябрь 2020"]
            }

@app.get(settings.URL_PREFIX_V1 + "/admin_dashboard")
async def admin_dashboard():
    """ Получение данных для дашборба главного врача
    """
    with open('admin.json','r', encoding='utf-8') as file:
        data = json.load(file, encoding='utf-8')
        data['patientsSurvay'] = get_patients_survay()
        data['preventions'] = get_hospital_preventions()
        data['receiptTime'] = get_receipt_doctor_time()
        data['death'] = get_death_percent()
        return data

####################### end Поинты работы с графиками ##########################


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=settings.PORT, log_level="info", reload=False)
